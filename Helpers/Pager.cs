﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlaicWebApplication.DataTransfer;

namespace Helpers
{
	public class Pager<T> : IEnumerable<T>
	{
		IEnumerable<T> _collection;
		private int _recordCount;
		private int _pageNumber;
		private int _pageSize;

		public int PageCount
		{
			get { return (_recordCount + _pageSize - 1) / _pageSize; }
		}

		public int PageNumber
		{
			get { return _pageNumber; }
			set { _pageNumber = value; }
		}

		public string ToHtml()
		{
			StringBuilder html = new StringBuilder();
			html.Append("<div class=\"pagination-container\"><ul class=\"pagination\">");

			if (_pageNumber > 1)
				html.Append(String.Format("<li class=\"PagedList-skipToPrevious\"><a href=\"#\" class=\"pagerbutton\" data-pagenumber=\"{0}\" rel=\"prev\">«</a></li>", _pageNumber - 1));

			for (int i = 1; i <= PageCount; i++)
				if (_pageNumber == i)
					html.Append(String.Format("<li class=\"active\"><a href=\"#\" class=\"pagerbutton\" data-pagenumber=\"{0}\">{1}</a></li>", i, i));
				else
					html.Append(String.Format("<li><a href=\"#\" class=\"pagerbutton\" data-pagenumber=\"{0}\">{1}</a></li>", i, i));

			if (_pageNumber < PageCount)
				html.Append(String.Format("<li class=\"PagedList-skipToNext\"><a href=\"#\" class=\"pagerbutton\" data-pagenumber=\"{0}\" rel=\"next\">»</a></li>", _pageNumber + 1));

			html.Append("</ul></div>");
			return html.ToString();
		}

		public Pager(IEnumerable<T> collection, int recordCount, int pageNumber, int pageSize)
		{
			_collection = collection;
			_recordCount = recordCount;
			_pageNumber = pageNumber;
			_pageSize = pageSize;
		}

		public IEnumerator<T> GetEnumerator()
		{
			return _collection.GetEnumerator();
		}

		System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
		{
			return this.GetEnumerator();
		}
	}
}
