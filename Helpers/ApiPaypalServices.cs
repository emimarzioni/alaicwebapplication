﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace AlaicWebApplication.Helpers
{
    public class ApiPaypalServices : IApiPaypalService
    {
        // Client
        HttpClient _client;

        // Constants
        const string CREATE_PAYMENT_URL = "http://localhost:44385/Payment/CreatePayment";
        const string EXECUTE_PAYMENT_URL = "http://localhost:44385/Payment/ExecutePayment";

        readonly Dictionary<string, string> Configs = new Dictionary<string, string>()
        {
            { "clientId", "AeWXEiHoQhQEFDKjz4-wBtfC_ePwE5O0b0ajp7uC5rfFDDME1FN-kddhVLJufSyKAV32JBs8Y8Iuwl5q" },
            { "clientSecret", "ECje7mRNTGlTHyF38R-ithUbjrukp8NpmqjgLCPGkAmqGeje089UoS8Ur2REJdIxQTmJdDadvUUQbC6w" }
        };

        // Constructor
        public ApiPaypalServices()
        {
            // Sandbox: https://api.sandbox.paypal.com
            // Live: https://api.paypa
            _client = new HttpClient();
            _client.BaseAddress = new Uri("https://api.sandbox.paypal.com/v1/");
            _client.DefaultRequestHeaders.Accept.Clear();
            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/x-www-form-urlencoded"));
        }


        // Public methods.
        public string GetAccessToken()
        {
            //This is the key section you were missing    
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes($"{Configs["clientId"]}:{Configs["clientSecret"]}");
            string val = System.Convert.ToBase64String(plainTextBytes);
            
            var byteArray = Encoding.ASCII.GetBytes($"{Configs["clientId"]}:{Configs["clientSecret"]}");
            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));

            //_client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", val);
            //_client.DefaultRequestHeaders.Add("Authorization", "Basic " + val);

            HttpResponseMessage httpResponse = _client.GetAsync("oauth2/token").Result;

            var response = string.Empty;
            if (httpResponse.IsSuccessStatusCode)
            {
                response = httpResponse.Content.ReadAsStringAsync().Result;
            }

            return response;
        }

        //public string PostRequest( string resource, string token)
        //{


        //}
    }
}
