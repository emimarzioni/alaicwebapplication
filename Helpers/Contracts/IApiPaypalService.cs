﻿namespace AlaicWebApplication.Helpers
{
    public interface IApiPaypalService
    {
        string GetAccessToken();
    }
}
