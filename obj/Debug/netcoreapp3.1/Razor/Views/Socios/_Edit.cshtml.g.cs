#pragma checksum "C:\Users\emima\source\repos\AlaicWebApplication\Views\Socios\_Edit.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "ae3b0da52052d196ef78f19daeda304e9e16dc0e"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Socios__Edit), @"mvc.1.0.view", @"/Views/Socios/_Edit.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\emima\source\repos\AlaicWebApplication\Views\_ViewImports.cshtml"
using AlaicWebApplication;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\emima\source\repos\AlaicWebApplication\Views\_ViewImports.cshtml"
using AlaicWebApplication.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"ae3b0da52052d196ef78f19daeda304e9e16dc0e", @"/Views/Socios/_Edit.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"8ba2e8375b0921ea525d3e09c3d050939d37c867", @"/Views/_ViewImports.cshtml")]
    public class Views_Socios__Edit : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<AlaicWebApplication.DataTransfer.SocioDTO>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\r\n");
#nullable restore
#line 3 "C:\Users\emima\source\repos\AlaicWebApplication\Views\Socios\_Edit.cshtml"
 using (Html.BeginForm("Edit", "Socios", FormMethod.Post))
{

#line default
#line hidden
#nullable disable
            WriteLiteral("    <div class=\"modal-dialog\">\r\n\t\t<div class=\"modal-content\">\r\n\t\t\t<div class=\"modal-header\">\r\n\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>\r\n\t\t\t\t<h4 class=\"modal-title\" id=\"myModalLabel\">\r\n\t\t\t\t\t");
#nullable restore
#line 10 "C:\Users\emima\source\repos\AlaicWebApplication\Views\Socios\_Edit.cshtml"
               Write(ViewBag.PopupTitle);

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n\t\t\t\t</h4>\r\n\t\t\t</div>\r\n\r\n            <div class=\"modal-body\">\r\n\r\n                ");
#nullable restore
#line 16 "C:\Users\emima\source\repos\AlaicWebApplication\Views\Socios\_Edit.cshtml"
           Write(Html.HiddenFor(x => x.Id));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n\r\n                <div class=\"form-group\">\r\n                    ");
#nullable restore
#line 19 "C:\Users\emima\source\repos\AlaicWebApplication\Views\Socios\_Edit.cshtml"
               Write(Html.LabelFor(x => x.Apellido));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                    ");
#nullable restore
#line 20 "C:\Users\emima\source\repos\AlaicWebApplication\Views\Socios\_Edit.cshtml"
               Write(Html.TextBoxFor(x => x.Apellido, new { @class = "form-control input-sm", maxlength = 50, text = new { transform = "uppercase" } }));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                </div>\r\n                <div class=\"form-group\">\r\n                    ");
#nullable restore
#line 23 "C:\Users\emima\source\repos\AlaicWebApplication\Views\Socios\_Edit.cshtml"
               Write(Html.LabelFor(x => x.Nombres));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                    ");
#nullable restore
#line 24 "C:\Users\emima\source\repos\AlaicWebApplication\Views\Socios\_Edit.cshtml"
               Write(Html.TextBoxFor(x => x.Nombres, new { @class = "form-control input-sm", maxlength = 50, text = new { transform = "uppercase" } }));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                </div>\r\n\r\n\r\n                <div class=\"form-group\">\r\n                    ");
#nullable restore
#line 29 "C:\Users\emima\source\repos\AlaicWebApplication\Views\Socios\_Edit.cshtml"
               Write(Html.LabelFor(x => x.Sexo));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                    ");
#nullable restore
#line 30 "C:\Users\emima\source\repos\AlaicWebApplication\Views\Socios\_Edit.cshtml"
               Write(Html.DropDownListFor(model => model.Sexo, new SelectList(new string[2] { "F", "M" }), htmlAttributes: new { @class = "form-control" }));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                </div>\r\n\r\n                <div class=\"form-group\">\r\n                    ");
#nullable restore
#line 34 "C:\Users\emima\source\repos\AlaicWebApplication\Views\Socios\_Edit.cshtml"
               Write(Html.LabelFor(x => x.FechaNacimiento));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                    ");
#nullable restore
#line 35 "C:\Users\emima\source\repos\AlaicWebApplication\Views\Socios\_Edit.cshtml"
               Write(Html.EditorFor(model => model.FechaNacimiento, new { htmlAttributes = new { @class = "form-control" } }));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                </div>\r\n\r\n                <div class=\"form-group\">\r\n                    ");
#nullable restore
#line 39 "C:\Users\emima\source\repos\AlaicWebApplication\Views\Socios\_Edit.cshtml"
               Write(Html.LabelFor(x => x.Domicilio));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                    ");
#nullable restore
#line 40 "C:\Users\emima\source\repos\AlaicWebApplication\Views\Socios\_Edit.cshtml"
               Write(Html.EditorFor(model => model.Domicilio, new { htmlAttributes = new { @class = "form-control" } }));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                    ");
#nullable restore
#line 41 "C:\Users\emima\source\repos\AlaicWebApplication\Views\Socios\_Edit.cshtml"
               Write(Html.ValidationMessageFor(model => model.Domicilio, "", new { @class = "text-danger" }));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                </div>\r\n\r\n                <div class=\"form-group\">\r\n                    ");
#nullable restore
#line 45 "C:\Users\emima\source\repos\AlaicWebApplication\Views\Socios\_Edit.cshtml"
               Write(Html.LabelFor(x => x.CodigoEstado));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                    ");
#nullable restore
#line 46 "C:\Users\emima\source\repos\AlaicWebApplication\Views\Socios\_Edit.cshtml"
               Write(Html.DropDownListFor(model => model.CodigoEstado,
                        new SelectList(Html.GetEnumSelectList<AlaicWebApplication.DataTransfer.SocioDTO.EstadoEnum>(), "Value", "Text"),
                        htmlAttributes: new { @class = "form-control" }));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                </div>\r\n\r\n                <div class=\"form-group\">\r\n                    ");
#nullable restore
#line 52 "C:\Users\emima\source\repos\AlaicWebApplication\Views\Socios\_Edit.cshtml"
               Write(Html.LabelFor(x => x.AñoIncorporacion));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                    ");
#nullable restore
#line 53 "C:\Users\emima\source\repos\AlaicWebApplication\Views\Socios\_Edit.cshtml"
               Write(Html.EditorFor(model => model.AñoIncorporacion, new { htmlAttributes = new { @class = "form-control" } }));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                </div>\r\n\r\n                <div class=\"form-group\">\r\n                    ");
#nullable restore
#line 57 "C:\Users\emima\source\repos\AlaicWebApplication\Views\Socios\_Edit.cshtml"
               Write(Html.LabelFor(x => x.Email));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                    ");
#nullable restore
#line 58 "C:\Users\emima\source\repos\AlaicWebApplication\Views\Socios\_Edit.cshtml"
               Write(Html.EditorFor(model => model.Email, new { htmlAttributes = new { @class = "form-control" } }));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                </div>\r\n\r\n                <div class=\"form-group\">\r\n                    ");
#nullable restore
#line 62 "C:\Users\emima\source\repos\AlaicWebApplication\Views\Socios\_Edit.cshtml"
               Write(Html.LabelFor(x => x.InstitucionId));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                    ");
#nullable restore
#line 63 "C:\Users\emima\source\repos\AlaicWebApplication\Views\Socios\_Edit.cshtml"
               Write(Html.EditorFor(model => model.InstitucionId, new { htmlAttributes = new { @class = "form-control" } }));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                </div>\r\n\r\n\r\n                <div class=\"form-group\">\r\n                    ");
#nullable restore
#line 68 "C:\Users\emima\source\repos\AlaicWebApplication\Views\Socios\_Edit.cshtml"
               Write(Html.LabelFor(x => x.EmailAlternativo));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                    ");
#nullable restore
#line 69 "C:\Users\emima\source\repos\AlaicWebApplication\Views\Socios\_Edit.cshtml"
               Write(Html.EditorFor(model => model.EmailAlternativo, new { htmlAttributes = new { @class = "form-control" } }));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                </div>\r\n\r\n                <div class=\"form-group\">\r\n                    ");
#nullable restore
#line 73 "C:\Users\emima\source\repos\AlaicWebApplication\Views\Socios\_Edit.cshtml"
               Write(Html.LabelFor(x => x.Pais));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                    ");
#nullable restore
#line 74 "C:\Users\emima\source\repos\AlaicWebApplication\Views\Socios\_Edit.cshtml"
               Write(Html.DropDownListFor(model => model.Pais,
                                                new SelectList(Html.GetEnumSelectList<AlaicWebApplication.DataTransfer.SocioDTO.PaisEnum>(), "Value", "Text"),
                                                htmlAttributes: new { @class = "form-control"}));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                </div>  \r\n\r\n                <div class=\"form-group\">\r\n                    ");
#nullable restore
#line 80 "C:\Users\emima\source\repos\AlaicWebApplication\Views\Socios\_Edit.cshtml"
               Write(Html.LabelFor(x => x.EsAsociacionNacional));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                    ");
#nullable restore
#line 81 "C:\Users\emima\source\repos\AlaicWebApplication\Views\Socios\_Edit.cshtml"
               Write(Html.DropDownListFor(model => model.EsAsociacionNacional,
                                                    new SelectList(Html.GetEnumSelectList<AlaicWebApplication.DataTransfer.SocioDTO.AsociacionNacionalEnum>(), "Value", "Text"),
                                                    htmlAttributes: new { @class = "form-control" }));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                </div>\r\n\r\n            </div>\r\n\r\n");
#nullable restore
#line 88 "C:\Users\emima\source\repos\AlaicWebApplication\Views\Socios\_Edit.cshtml"
             if (!ViewData.ModelState.IsValid)
			{

#line default
#line hidden
#nullable disable
            WriteLiteral("\t\t\t\t<script type=\"text/javascript\">\r\n\t\t\t\t\tvar errors = ");
#nullable restore
#line 91 "C:\Users\emima\source\repos\AlaicWebApplication\Views\Socios\_Edit.cshtml"
                            Write(Html.Raw(Json.Serialize(string.Join(Environment.NewLine,ViewData.ModelState
                        .Where(x => x.Value.Errors.Count > 0)
                        .SelectMany(x => x.Value.Errors)
                        .Select(error => error.ErrorMessage)))));

#line default
#line hidden
#nullable disable
            WriteLiteral(";\r\n\t\t\t\t\tbootbox.alert(errors);\r\n\t\t\t\t</script>\r\n");
#nullable restore
#line 97 "C:\Users\emima\source\repos\AlaicWebApplication\Views\Socios\_Edit.cshtml"
			}

#line default
#line hidden
#nullable disable
            WriteLiteral("\t\t\t<div class=\"modal-footer\">\r\n\t\t\t\t<button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Cerrar</button>\r\n\t\t\t\t<button type=\"button\" class=\"btn btn-primary\" id=\"btn-submit\">Guardar</button>\r\n\t\t\t</div>\r\n\t\t</div>\r\n    </div>\r\n");
#nullable restore
#line 104 "C:\Users\emima\source\repos\AlaicWebApplication\Views\Socios\_Edit.cshtml"
}

#line default
#line hidden
#nullable disable
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<AlaicWebApplication.DataTransfer.SocioDTO> Html { get; private set; }
    }
}
#pragma warning restore 1591
