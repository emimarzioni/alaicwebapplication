﻿using AlaicWebApplication.DataTransfer;
using AlaicWebApplication.Display.Interfaces;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using static AlaicWebApplication.DataTransfer.SocioDTO;

namespace AlaicWebApplication.Areas.Identity.Pages.Account
{
    [AllowAnonymous]
    public class RegisterModel : PageModel
    {
        private readonly SignInManager<IdentityUser> _signInManager;
        private readonly UserManager<IdentityUser> _userManager;
        private readonly ILogger<RegisterModel> _logger;
        private readonly IEmailSender _emailSender;
        private readonly ISocioSettings _socioSettings;
        private readonly InstitucionIntfSettings _institucionSettings;


        public RegisterModel(
            UserManager<IdentityUser> userManager,
            SignInManager<IdentityUser> signInManager,
            ILogger<RegisterModel> logger,
            IEmailSender emailSender,
            ISocioSettings socioSettings,
            InstitucionIntfSettings institucionSettings)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _logger = logger;
            _emailSender = emailSender;
            _socioSettings = socioSettings;
            _institucionSettings = institucionSettings;
        }

        [BindProperty]
        public InputModel Input { get; set; }

        public string ReturnUrl { get; set; }

        public IList<AuthenticationScheme> ExternalLogins { get; set; }


        public IList<InstitucionDTO> Instituciones { get; set; }

        [BindProperty(SupportsGet = true)]
        public string SearchString { get; set; }
        // Requires using Microsoft.AspNetCore.Mvc.Rendering;
        public SelectList TiposInstituciones { get; set; }
        [BindProperty(SupportsGet = true)]
        public string tipoInstitucion { get; set; }

        public class InputModel
        {
            [Required]
            [EmailAddress]
            [Display(Name = "Email")]
            public string Email { get; set; }

            [Required]
            [StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 6)]
            [DataType(DataType.Password)]
            [Display(Name = "Password")]
            public string Password { get; set; }

            [DataType(DataType.Password)]
            [Display(Name = "Confirm password")]
            [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
            public string ConfirmPassword { get; set; }

            [Required(ErrorMessage = "El apellido es requerido")]
            [DataType(DataType.Text)]
            [Display(Name = "Apellido")]
            public string Apellido { get; set; }

            [Required(ErrorMessage = "El nombre es requerido")]
            [DataType(DataType.Text)]
            [Display(Name = "Nombres")]
            public string Nombres { get; set; }

            [Required(ErrorMessage = "La fecha de Nacimiento es requerida")]
            [DataType(DataType.DateTime)]
            [Display(Name = "Fecha de Nacimiento")]
            public DateTime FechaNacimiento { get; set; }

            [Required(ErrorMessage = "El domicilio es requerido")]
            [DataType(DataType.Text)]
            [Display(Name = "Domicilio")]
            public string Domicilio { get; set; }

            [Required(ErrorMessage = "El Año de Incorporación es requerido")]
            [DataType(DataType.Text)]
            [Display(Name = "Año de Incorporación")]
            public int AnioIncorporacion { get; set; }

            [Required(ErrorMessage = "El campo Sexo es requerido")]
            [DataType(DataType.Text)]
            [Display(Name = "Sexo")]
            public char Sexo { get; set; }

            [Required(ErrorMessage = "El Estado del Socio es requerido")]
            [DataType(DataType.Text)]
            [Display(Name = "Estado del socio")]
            public EstadoEnum CodigoEstado { get; set; }
        }

        public async Task OnGetAsync(string returnUrl = null)
        {
            var instituciones = _institucionSettings.GetInstituciones(null);

            List<string> tipoInstitucionQuery = instituciones.Select(x => x.TipoInstitucion).Distinct().ToList();

            if (!string.IsNullOrEmpty(SearchString))
            {
                instituciones = instituciones.Where(i => i.Nombre.Contains(SearchString)).ToList();
            }
            else
            {
                instituciones = instituciones.ToList();
            }

            if (!string.IsNullOrEmpty(tipoInstitucion))
            {
                instituciones = instituciones.Where(i => i.TipoInstitucion == tipoInstitucion).ToList();
            }

            TiposInstituciones = new SelectList(tipoInstitucionQuery.Distinct().ToList());

            ReturnUrl = returnUrl;
            ExternalLogins = (await _signInManager.GetExternalAuthenticationSchemesAsync()).ToList();
        }

        public async Task<IActionResult> OnPostAsync(string returnUrl = null)
        {
            returnUrl = returnUrl ?? Url.Content("~/");
            ExternalLogins = (await _signInManager.GetExternalAuthenticationSchemesAsync()).ToList();
            if (ModelState.IsValid)
            {
                var user = new IdentityUser { UserName = Input.Email, Email = Input.Email };
                var result = await _userManager.CreateAsync(user, Input.Password);

                var socio = new SocioDTO()
                {
                    Apellido = Input.Apellido,
                    Nombres = Input.Nombres,
                    FechaNacimiento = Input.FechaNacimiento,
                    AñoIncorporacion = Input.AnioIncorporacion,
                    Email = Input.Email,
                    Sexo = Input.Sexo
                };
                bool success = _socioSettings.SetSocio(new SocioDTO());

                if (result.Succeeded)
                {
                    _logger.LogInformation("User created a new account with password.");

                    var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                    code = WebEncoders.Base64UrlEncode(Encoding.UTF8.GetBytes(code));
                    var callbackUrl = Url.Page(
                        "/Account/ConfirmEmail",
                        pageHandler: null,
                        values: new { area = "Identity", userId = user.Id, code = code },
                        protocol: Request.Scheme);

                    await _emailSender.SendEmailAsync(Input.Email, "Confirm your email",
                        $"Please confirm your account by <a href='{HtmlEncoder.Default.Encode(callbackUrl)}'>clicking here</a>.");

                    if (_userManager.Options.SignIn.RequireConfirmedAccount)
                    {
                        return RedirectToPage("RegisterConfirmation", new { email = Input.Email });
                    }
                    else
                    {
                        await _signInManager.SignInAsync(user, isPersistent: false);
                        return LocalRedirect(returnUrl);
                    }
                }
                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError(string.Empty, error.Description);
                }
            }

            // If we got this far, something failed, redisplay form
            return Page();
        }
    }
}
