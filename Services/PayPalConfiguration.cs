﻿using PayPal.Api;
using System.Collections.Generic;

namespace AlaicWebApplication.Services
{
    public class PayPalConfiguration : IPayPalConfiguration
    {
        readonly string _clientId;
        readonly string _clientSecret;

        Helpers.IApiPaypalService _apiPaypalService;

        // Create the configuration map that contains mode and other optional configuration details.
        readonly Dictionary<string, string> Configs = new Dictionary<string, string>() 
        {
            { "clientId", "AeWXEiHoQhQEFDKjz4-wBtfC_ePwE5O0b0ajp7uC5rfFDDME1FN-kddhVLJufSyKAV32JBs8Y8Iuwl5q" },
            { "clientSecret", "ECje7mRNTGlTHyF38R-ithUbjrukp8NpmqjgLCPGkAmqGeje089UoS8Ur2REJdIxQTmJdDadvUUQbC6w" }
        };

        // Static constructor for setting the readonly static members.
        public PayPalConfiguration(Helpers.IApiPaypalService apiPaypalService)
        {
            _apiPaypalService = apiPaypalService;

            _clientId = Configs.GetValueOrDefault("clientId");
            _clientSecret = Configs.GetValueOrDefault("clientSecret");
        }


        // Create accessToken
        private string GetAccessToken()
        {
            // ###AccessToken
            // Retrieve the access token from OAuthTokenCredential by passing in
            // ClientID and ClientSecret
            // It is not mandatory to generate Access Token on a per call basis.
            // Typically the access token can be generated once and reused within the expiry window                
            string accessToken = new OAuthTokenCredential(_clientId, _clientSecret).GetAccessToken();
            return accessToken;
        }

        // Returns APIContext object
        public APIContext GetAPIContext(string accessToken)
        {
            var at = new OAuthTokenCredential(_clientId, _clientSecret, new Dictionary<string, string>()
        {
            { "clientId", "AeWXEiHoQhQEFDKjz4-wBtfC_ePwE5O0b0ajp7uC5rfFDDME1FN-kddhVLJufSyKAV32JBs8Y8Iuwl5q" },
            { "clientSecret", "ECje7mRNTGlTHyF38R-ithUbjrukp8NpmqjgLCPGkAmqGeje089UoS8Ur2REJdIxQTmJdDadvUUQbC6w" }
        });
            //var tuvi = at.GetAccessToken();

            // Pass in a `APIContext` object to authenticate 
            // the call and to send a unique request id 
            // (that ensures idempotency). The SDK generates
            // a request id if you do not pass one explicitly. 
            var apiContext = new APIContext(string.IsNullOrEmpty(accessToken) ? GetAccessToken() : accessToken);
            apiContext.Config = Configs;

            return apiContext;
        }
    }
}
