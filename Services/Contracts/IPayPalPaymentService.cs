﻿using PayPal.Api;

namespace AlaicWebApplication.Services
{
    public interface IPayPalPaymentService
    {
        Payment CreatePayment(string baseUrl, string intent);

        Payment ExecutePayment(string paymentId, string payerId);

        Capture CapturePayment(string paymentId);

        //string GetRandomInvoiceNumber();
    }
}
