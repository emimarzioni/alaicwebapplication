﻿using PayPal.Api;

namespace AlaicWebApplication.Services
{
    public interface IPayPalConfiguration
    {
        APIContext GetAPIContext(string accessToken);
    }
}
