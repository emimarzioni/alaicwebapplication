﻿using System.Diagnostics;
using AlaicWebApplication.Entities;
using Microsoft.EntityFrameworkCore;

namespace AlaicWebApplication.DataAccess
{
	public class AlaicContext : DbContext
	{
		public AlaicContext() {
		
		}
		public AlaicContext(DbContextOptions options) : base(options)
		{
			
		}
		public DbSet<Socio> Socios { get; set; }
		public DbSet<Cuota> Cuotas { get; set; }
        public DbSet<Institucion> Instituciones { get; set; }

		protected override void OnModelCreating(ModelBuilder modelBuilder) 
		{
			base.OnModelCreating(modelBuilder);
		}
    }
}