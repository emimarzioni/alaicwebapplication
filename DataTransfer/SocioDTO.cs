﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace AlaicWebApplication.DataTransfer
{
	[DataContract]
	public class SocioDTO
    {
		public enum AsociacionNacionalEnum
		{
			Sí = 1,
			No = 2
		}
		public enum PaisEnum
		{
			Argentina = 1,
			Brasil = 2,
			Perú = 3,
			Chile = 4,
			Uruguay = 5,
			Paraguay = 6,
			Venezuela = 7,
			Ecuador = 8,
			Colombia = 9
		}
		public enum EstadoEnum
		{
			Activo = 1,
			Vitalicio = 2,
			Suspendido = 3
		}

		[DataMember(Name = "id")]
		[DisplayName("Id")]
		public int Id { get; set; }

		[Required(ErrorMessage = "El apellido es requerido")]
		[DataMember(Name = "ipellido")]
		[DisplayName("Apellido")]
		public string Apellido { get; set; }

		[DataMember(Name = "nombres")]
		[Required(ErrorMessage = "El nombre es requerido")]
		[DisplayName("Nombres")]
		public string Nombres { get; set; }

		[DataMember(Name = "sexo")]
		[DisplayName("Sexo")]
		public char Sexo { get; set; }

		[DataMember(Name = "fechaNacimiento")]
		[DisplayName("Fecha de Nacimiento")]
		public DateTime FechaNacimiento { get; set; }

		[DataMember(Name = "Domicilio")]
		[DisplayName("Domicilio")]
		public string Domicilio { get; set; }

		[DataMember(Name = "codigoEstado")]
		[DisplayName("Estado del socio")]
        public EstadoEnum CodigoEstado { get; set; }

		[DataMember(Name = "añoIncorporacion")]
		[DisplayName("Año de incorporación")]
		public int AñoIncorporacion { get; set; }

		[DataMember(Name = "email")]
		[DisplayName("E-mail")]
		public string Email { get; set; }

		[DataMember(Name = "institucionId")]
		[DisplayName("Institución")]
		public int InstitucionId { get; set; }

		[DataMember(Name = "institucion")]
		[DisplayName("Institución")]
		public InstitucionDTO Institucion { get; set; }

		[DataMember(Name = "emailAlternativo")]
		[DisplayName("E-mail alternativo")]
		public string EmailAlternativo { get; set; }

		[DataMember(Name = "pais")]
		[DisplayName("País")]
        public PaisEnum Pais { get; set; }

		[DataMember(Name = "esAsociacionNacional")]
		[DisplayName("Es Asociación Nacional")]
		public AsociacionNacionalEnum EsAsociacionNacional { get; set; }

		[DataMember(Name = "cuotas")]
		public List<CuotaDTO> Cuotas { get; set; } = new List<CuotaDTO>();	
	}
    
}