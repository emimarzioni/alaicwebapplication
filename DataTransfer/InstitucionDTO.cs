﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;


namespace AlaicWebApplication.DataTransfer
{
    [DataContract]
    public class InstitucionDTO
    {
        public enum TipoInstitucionEnum
        {
            Universidad = 1,
            [Display(Name = "Centro de Estudios")]
            CentroDeEstudios = 2,
            [Display(Name = "Asociación")]
            Asociación = 3,
            Instituto = 4,
            Otra = 5
        }

        [DataMember(Name = "id")]
        [DisplayName("Cod. Institución")]
        public int Id { get; set; }

        [Required]
        [DataMember(Name = "nombre")]
        [DisplayName("Nombre")]
        public string Nombre { get; set; }

        [Required]
        [DataMember(Name = "tipo_institucion")]
        [DisplayName("Tipo Institución")]
        public TipoInstitucionEnum Tipo { get; set; }

        [Required]
        [DataMember(Name = "tipo_institucionString")]
        [DisplayName("Institución")]
        public string TipoInstitucion { get; set; }
    }
}
