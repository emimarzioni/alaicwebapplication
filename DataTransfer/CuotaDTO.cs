﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace AlaicWebApplication.DataTransfer
{
    [DataContract]
    public class CuotaDTO
	{
        public enum TipoMonedaEnum
        {
            Pesos = 1,
            Dolares = 2,
            Reales = 3
        }

        [DataMember(Name = "Id")]
        public int Id { get; set; }

        [DataMember(Name = "socioId")]
        [DisplayName("Socio ID")]
        public int SocioId { get; set; }

        [DataMember(Name = "socio")]
        public SocioDTO Socio { get; set; }

        [DataMember(Name = "año")]
        [Required(ErrorMessage = "El año es requerido")]
        [DisplayName("Año")]
        public int AñoPagado { get; set; }

        [DataMember(Name = "monto")]
        [DisplayName("Monto")]
        public double Monto { get; set; }

        [DataMember(Name = "tipoMoneda")]
        [DisplayName("Tipo de Moneda")]
        public TipoMonedaEnum TipoMoneda { get; set; }
		public DateTime FechaEmision { get; set; }

        [DataMember(Name = "fechaVencimiento")]
        [DisplayName("Fecha de Vencimiento")]
        public DateTime FechaVencimiento { get; set; }

        [DataMember(Name = "fechaPago")]
        [DisplayName("Fecha de Pago")]
        public DateTime? FechaPago { get; set; }
	}
}