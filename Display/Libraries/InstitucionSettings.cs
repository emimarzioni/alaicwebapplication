﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AlaicWebApplication.DataTransfer;
using AlaicWebApplication.Entities;
using AlaicWebApplication.Display.Interfaces;
using AlaicWebApplication.DataAccess;
using Helpers;
using static AlaicWebApplication.DataTransfer.InstitucionDTO;

namespace AlaicWebApplication.Display.Libraries
{
    public class InstitucionSettings : InstitucionIntfSettings
    {
        private readonly AlaicContext _context;

        public InstitucionSettings(AlaicContext context)
        {
            this._context = context;
        }
        public InstitucionDTO GetInstitucion(string description = null) {
            var predicate = PredicateBuilder.True<Institucion>();

            predicate = predicate.And(s => s.Nombre.Contains(description));

            var institucion = _context.Instituciones
                            .Where(predicate)
                            .Select(i => new InstitucionDTO()
                            {
                                Nombre = i.Nombre,
                                Tipo = (InstitucionDTO.TipoInstitucionEnum)Enum.Parse(typeof(Institucion.TipoInstitucionEnum), i.Tipo.ToString()),
                                Id = i.ID,
                                TipoInstitucion = i.Tipo.ToString()
                            }).FirstOrDefault();
            return institucion;
        }
        public List<InstitucionDTO> GetInstituciones(string description = null) {
            
            var predicate = PredicateBuilder.True<Institucion>();

            if (!string.IsNullOrEmpty(description))
                predicate = predicate.And(s => s.Nombre.Contains(description));

            var instituciones = _context.Instituciones
                            .Where(predicate)
                            .Select(i => new InstitucionDTO()
                            {
                                Nombre = i.Nombre,
                                Id = i.ID,
                            }).ToList();

            foreach (var item in instituciones)
            {
                item.Tipo = (TipoInstitucionEnum)Enum.Parse(typeof(Institucion.TipoInstitucionEnum), item.Tipo.ToString());
                item.TipoInstitucion = item.Tipo.ToString();
            }
            return instituciones;
        }

        public List<InstitucionDTO> GetInstituciones(TipoInstitucionEnum tipoInstitucion)
        {
            throw new NotImplementedException();
        }
    }
}
