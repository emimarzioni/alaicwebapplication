﻿using AlaicWebApplication.DataTransfer;
using AlaicWebApplication.Display.Interfaces;
using AlaicWebApplication.Entities;
using AlaicWebApplication.DataAccess;
using Helpers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AlaicWebApplication.Display.Libraries
{
    public class SocioSettings : ISocioSettings
    {
        private readonly AlaicContext context;

        public SocioSettings(AlaicContext context)
        {
            this.context = context;
        }

        public bool DeteleSocio(int nroSocio)
        {
            bool deletedSuccess = false;
            try
            {
                var socio = context.Socios.FirstOrDefault(s => s.Id == nroSocio);
                context.Socios.Remove(socio);
                context.SaveChanges();
                deletedSuccess = true;
            }
            catch (Exception ex)
            {
                deletedSuccess = false;
            }
            return deletedSuccess;
        }

        public SocioDTO GetSocio(int nroSocio)
        {
            var predicate = PredicateBuilder.True<Socio>();

            predicate = predicate.And(s => s.Id == nroSocio);

            InstitucionIntfSettings institucionesSettings = new InstitucionSettings(context);
            var instituciones = institucionesSettings.GetInstituciones(null);

            var socio = context.Socios
                            .Where(predicate)
                            .Select(s => new SocioDTO()
                            {
                                Apellido = s.Apellido,
                                Nombres = s.Nombres,
                                Email = s.Email,
                                Pais = (SocioDTO.PaisEnum)Enum.Parse(typeof(Socio.PaisEnum), s.Pais.ToString()),
                                AñoIncorporacion = s.AñoIncorporacion,
                                Domicilio = s.Domicilio,
                                Sexo = s.Sexo,
                                Id = s.Id,
                                InstitucionId = s.InstitucionId
                            }).FirstOrDefault();

            socio.Institucion = instituciones.FirstOrDefault(x => x.Id == socio.InstitucionId);
            return socio;
        }

        public SocioDTO GetSocio(string email)
        {
            var predicate = PredicateBuilder.True<Socio>();
            predicate = predicate.And(s => s.Email == email);

            InstitucionIntfSettings institucionesSettings = new InstitucionSettings(context);
            var instituciones = institucionesSettings.GetInstituciones(null);

            var socio = context.Socios
                            .Where(predicate)
                            .Select(s => new SocioDTO()
                            {
                                Apellido = s.Apellido,
                                Nombres = s.Nombres,
                                Email = s.Email,
                                Pais = (SocioDTO.PaisEnum)Enum.Parse(typeof(Socio.PaisEnum), s.Pais.ToString()),
                                AñoIncorporacion = s.AñoIncorporacion,
                                Domicilio = s.Domicilio,
                                Sexo = s.Sexo,
                                Id = s.Id,
                                InstitucionId = s.InstitucionId
                            }).FirstOrDefault();
            socio.Institucion = instituciones.FirstOrDefault(x => x.Id == socio.InstitucionId);
            return socio;
        }

        public List<SocioDTO> GetSocios(string filter)
        {
            var predicate = PredicateBuilder.True<Socio>();

            if (!string.IsNullOrEmpty(filter))
            {
                predicate = predicate.And(s => s.Apellido.ToUpper().Contains(filter.ToUpper()));
            }

            InstitucionIntfSettings institucionesSettings = new InstitucionSettings(context);
            var instituciones = institucionesSettings.GetInstituciones(null);

            var sociosLst = context.Socios
                            .Where(predicate)
                            .Select(s => new SocioDTO()
                            {
                                Apellido = s.Apellido,
                                Nombres = s.Nombres,
                                Email = s.Email,
                                Pais = (SocioDTO.PaisEnum)Enum.Parse(typeof(Socio.PaisEnum), s.Pais.ToString()),
                                AñoIncorporacion = s.AñoIncorporacion,
                                Domicilio = s.Domicilio,
                                Sexo = s.Sexo,
                                InstitucionId = s.InstitucionId,
                                Id = s.Id
                            }).Take(30).ToList();

            foreach (var socio in sociosLst)
                socio.Institucion = instituciones.FirstOrDefault(x => x.Id == socio.InstitucionId);

            return sociosLst;
        }

        public List<SocioDTO> GetSocios()
        {
            return this.GetSocios(null);
        }

        public bool SetSocio(SocioDTO model)
        {
            bool isNew = false;
            try
            {
                var updatedSocio = context.Socios.FirstOrDefault(s => s.Id == model.Id);

                if (updatedSocio == null) {
                    updatedSocio = new Socio();
                    isNew = true;
                }

                updatedSocio.Apellido = model?.Apellido;
                updatedSocio.Nombres = model.Nombres;
                updatedSocio.Pais = (Socio.PaisEnum)Enum.Parse(typeof(Socio.PaisEnum), model.Pais.ToString());
                updatedSocio.AñoIncorporacion = model.AñoIncorporacion;
                updatedSocio.CodigoEstado = (Socio.EstadoEnum)Enum.Parse(typeof(Socio.EstadoEnum), model.CodigoEstado.ToString());
                updatedSocio.Domicilio = model.Domicilio;
                updatedSocio.Email = model.Email;
                updatedSocio.EmailAlternativo = model.EmailAlternativo;
                updatedSocio.EsAsociacionNacional = (Socio.AsociacionNacionalEnum)Enum.Parse(typeof(Socio.AsociacionNacionalEnum), model.EsAsociacionNacional.ToString());
                updatedSocio.FechaNacimiento = model.FechaNacimiento;
                updatedSocio.InstitucionId = model.InstitucionId;
                updatedSocio.Sexo = model.Sexo;
                updatedSocio.Pais = (Socio.PaisEnum)Enum.Parse(typeof(Socio.PaisEnum), model.Pais.ToString());

                if (isNew)
                {
                    context.Add(updatedSocio);
                    context.SaveChanges();

                    var cuotas = new List<Cuota>
                        {
                            new Cuota 
                            {
                                AñoPagado=DateTime.Now.Year,
                                Monto= 30,FechaEmision=DateTime.Now,
                                FechaVencimiento =  new DateTime(DateTime.Now.Year , 12, 31),
                                FechaPago = null,
                                SocioId = updatedSocio.Id,
                                TipoMoneda = Cuota.TipoMonedaEnum.Dolares 
                            },

                            new Cuota
                            { 
                                AñoPagado= (DateTime.Now.Year) + 1 ,
                                Monto= 30,
                                FechaEmision=DateTime.Now,
                                FechaVencimiento =  new DateTime(DateTime.Now.Year + 1 , 12, 31),
                                FechaPago = null,
                                SocioId = updatedSocio.Id,
                                TipoMoneda = Cuota.TipoMonedaEnum.Dolares 
                            }
                        };

                    cuotas.ForEach(s => context.Cuotas.Add(s));
                    context.SaveChanges();
                    return true;
                }
                else
                {
                    context.Update(updatedSocio);
                    context.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
                //Model
            }
        }
    }
}
