﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AlaicWebApplication.DataTransfer;
using AlaicWebApplication.Display.Interfaces;
using AlaicWebApplication.DataAccess;
using AlaicWebApplication.Entities;
using Helpers;
using System.Runtime.Serialization;

namespace AlaicWebApplication.Display.Libraries
{
    public class CuotaSettings : ICuotaSettings
    {
        private readonly AlaicContext _context;

        public CuotaSettings(AlaicContext context)
        {
            this._context = context;
        }

        public CuotaDTO GetCuota(int nroSocio, int anio)
        {
            ISocioSettings socioSettings = new SocioSettings(_context);
            var socio = socioSettings.GetSocio(nroSocio);

            var cuotasList = _context.Cuotas
                            .Where(s => s.SocioId == nroSocio && s.AñoPagado == anio)
                            .Select(c => new CuotaDTO()
                            {
                                AñoPagado = c.AñoPagado,
                                Monto = c.Monto,
                                FechaEmision = c.FechaEmision,
                                FechaVencimiento = c.FechaVencimiento,
                                FechaPago = c.FechaPago,
                                SocioId = c.SocioId,
                                Socio = socio,
                                TipoMoneda = (CuotaDTO.TipoMonedaEnum)Enum.Parse(typeof(Cuota.TipoMonedaEnum), c.TipoMoneda.ToString())
                            }).FirstOrDefault();

            return cuotasList;
        }

        public CuotaDTO GetCuota(long id)
        {
            var cuota = _context.Cuotas.FirstOrDefault(c => c.ID == id);
            ISocioSettings socioSettings = new SocioSettings(_context);

            var cuotaModel = new CuotaDTO()
            {
                AñoPagado = cuota.AñoPagado,
                Monto = cuota.Monto,
                FechaVencimiento = cuota.FechaVencimiento,
                FechaPago = cuota.FechaPago,
                TipoMoneda = (CuotaDTO.TipoMonedaEnum)Enum.Parse(typeof(Cuota.TipoMonedaEnum), cuota.TipoMoneda.ToString()),
                SocioId = cuota.SocioId,
                Socio = socioSettings.GetSocio(cuota.Socio.Id)
            };
            return cuotaModel;
        }

        public List<CuotaDTO> GetCuotas(int nroSocio)
        {
            ISocioSettings socioSettings = new SocioSettings(_context);

            var cuotasList = _context.Cuotas
                            .Where(s => s.SocioId == nroSocio)
                            .Select(c => new CuotaDTO()
                            {

                                AñoPagado = c.AñoPagado,
                                Monto = c.Monto,
                                FechaEmision = c.FechaEmision,
                                FechaVencimiento = c.FechaVencimiento,
                                FechaPago = c.FechaPago,
                                SocioId = c.SocioId,
                                Socio = socioSettings.GetSocio(nroSocio),
                                TipoMoneda = (CuotaDTO.TipoMonedaEnum)Enum.Parse(typeof(Cuota.TipoMonedaEnum), c.TipoMoneda.ToString())
                            }).Take(15).ToList();

            return cuotasList;
        }

        public List<CuotaDTO> GetCuotas(SocioDTO socio)
        {
            if (socio != null)
                return this.GetCuotas(socio.Id);
            return null;
        }

        public List<CuotaDTO> GetCuotas(string apellido, string anio = null)
        {
            var predicate = PredicateBuilder.True<Cuota>();
            int anioPagado = DateTime.Now.Year;

            if (!string.IsNullOrEmpty(anio))
                anioPagado = int.Parse(anio);

            if (!string.IsNullOrEmpty(apellido))
            {
                predicate = predicate.And(c => c.Socio.Nombres.ToUpper().Contains(apellido.ToUpper(), StringComparison.CurrentCulture))
                    .And((c => c.AñoPagado == anioPagado));
            }

            ISocioSettings socioSettings = new SocioSettings(_context);
            
            var cuotasList = _context.Cuotas
                            .Where(predicate)
                            .Select(c => new CuotaDTO()
                            {

                                AñoPagado = c.AñoPagado,
                                Monto = c.Monto,
                                FechaEmision = c.FechaEmision,
                                FechaVencimiento = c.FechaVencimiento,
                                FechaPago = c.FechaPago,
                                SocioId = c.SocioId,
                                Socio = socioSettings.GetSocio(c.SocioId),
                                TipoMoneda = (CuotaDTO.TipoMonedaEnum)Enum.Parse(typeof(Cuota.TipoMonedaEnum), c.TipoMoneda.ToString())
                            }).Take(15).ToList();

            return cuotasList;
        }

        public bool SetCuota(CuotaDTO model)
        {
            bool isNew = false;
            try
            {
                var cuota = _context.Cuotas.FirstOrDefault(c => c.ID == model.Id);

                if (cuota == null)
                {
                    cuota = new Cuota();
                    isNew = true;
                }

                cuota.AñoPagado = model.AñoPagado;
                cuota.TipoMoneda = (Cuota.TipoMonedaEnum)Enum.Parse(typeof(Cuota.TipoMonedaEnum), model.TipoMoneda.ToString());
                cuota.FechaVencimiento = model.FechaVencimiento;
                cuota.FechaEmision = cuota.FechaEmision;
                cuota.FechaPago = model.FechaPago;
                cuota.SocioId = model.SocioId;
                cuota.Socio = _context.Socios.FirstOrDefault(s=>s.Id == model.SocioId);

                if (isNew)
                {
                    _context.Add(cuota);
                    _context.SaveChanges();
                    return true ;
                }
                else
                {
                    _context.Update(cuota);
                    _context.SaveChanges();
                    return true ;
                }
            }
            catch (Exception ex)
            {
                //Model
                return false;
            }
        }

        public void SetCuotas(List<CuotaDTO> cuotas)
        {
            if (cuotas != null) {
                cuotas.ForEach(cu => this.SetCuota(cu));
            } 
        }
    }
}
