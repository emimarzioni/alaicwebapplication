﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AlaicWebApplication.DataTransfer;
using AlaicWebApplication.Display.Interfaces;
using AlaicWebApplication.DataAccess;
using AlaicWebApplication.Entities;
using Helpers;
using System.Runtime.Serialization;
using Microsoft.AspNetCore.Identity;

namespace AlaicWebApplication.Display.Libraries
{
    public class UserSettings : IUserSettings
    {
        private readonly AlaicContext _context;

        public UserSettings(AlaicContext alaicContext)
        {
            this._context = alaicContext;
        }

        public SocioDTO GetSocio(string socioEmail)
        {
            int socioId = _context.Socios.Where(x => x.Email == socioEmail).FirstOrDefault().Id;
            var socioModel = _context.Socios
                            .Where(s => s.Id == socioId)
                            .Select(s => new SocioDTO()
                            {
                                Apellido = s.Apellido,
                                Nombres = s.Nombres,
                                Email = s.Email,
                                Pais = (SocioDTO.PaisEnum)Enum.Parse(typeof(Socio.PaisEnum), s.Pais.ToString()),
                                AñoIncorporacion = s.AñoIncorporacion,
                                Domicilio = s.Domicilio,
                                Sexo = s.Sexo,
                                Id = s.Id
                            }).FirstOrDefault();
            return socioModel;
        }

        public SocioDTO GetSocio(IdentityUser loggedUser)
        {
            var socio = this.GetSocio(loggedUser.Email);
            return socio;
        }

        public int GetSocioID(IdentityUser loggedUser)
        {
            var socio = this.GetSocio(loggedUser);
            return socio.Id;
        }

        public int GetSocioID(string socioEmail)
        {
            var socio = this.GetSocio(socioEmail);
            return socio.Id;
        }
    }
}
