﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AlaicWebApplication.DataTransfer;

namespace AlaicWebApplication.Display.Interfaces
{
    public interface ISocioSettings
    {
        SocioDTO GetSocio(string email);
        SocioDTO GetSocio(int nroSocio);
        List<SocioDTO> GetSocios();
        List<SocioDTO> GetSocios(string filter);
        bool SetSocio(SocioDTO socio);
        bool DeteleSocio(int nroSocio);
    }
}
