﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AlaicWebApplication.DataTransfer;
using Microsoft.AspNetCore.Identity;

namespace AlaicWebApplication.Display.Interfaces
{
    public interface IUserSettings
    {
        int GetSocioID(IdentityUser loggedUser);
        int GetSocioID(string socioEmail);
        SocioDTO GetSocio(IdentityUser loggedUser);
        SocioDTO GetSocio(string socioEmail);
    }
}
