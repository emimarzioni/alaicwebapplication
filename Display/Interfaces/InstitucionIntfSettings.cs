﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AlaicWebApplication.DataTransfer;
using static AlaicWebApplication.DataTransfer.InstitucionDTO;

namespace AlaicWebApplication.Display.Interfaces
{
    public interface InstitucionIntfSettings
    {
        InstitucionDTO GetInstitucion(string description);
        List<InstitucionDTO> GetInstituciones(string description = null);
        List<InstitucionDTO> GetInstituciones(TipoInstitucionEnum tipoInstitucion);
    }
}