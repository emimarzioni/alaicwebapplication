﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AlaicWebApplication.DataTransfer;

namespace AlaicWebApplication.Display.Interfaces
{
    public interface ICuotaSettings
    {
        CuotaDTO GetCuota(long id);
        CuotaDTO GetCuota(int nroSocio, int anio);
        List<CuotaDTO> GetCuotas(SocioDTO socio);
        List<CuotaDTO> GetCuotas(string apellido, string anio = null);
        List<CuotaDTO> GetCuotas(int nroSocio);
        bool SetCuota(CuotaDTO cuota);
        void SetCuotas(List<CuotaDTO> cuotas);
    }
}
