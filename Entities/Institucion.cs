﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AlaicWebApplication.Entities
{
    [Table("Institucion")]
    public class Institucion : IEntity
    {
        public enum TipoInstitucionEnum
        {
            Universidad = 1,
            CentroDeEstudios = 2,
            Asociación = 3,
            Instituto = 4,
            Otra = 5
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key, Column(Order = 0)]
        public int ID { get; set; }

        [Required]
        public string Nombre { get; set; }

        [Required]
        public TipoInstitucionEnum Tipo { get; set; }

        public object GetId()
        {
            return this.ID;
        }
    }
}
