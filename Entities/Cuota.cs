﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AlaicWebApplication.Entities
{
	[Table("Cuota")]
	public class Cuota : IEntity
	{
		public enum TipoMonedaEnum
		{
			Pesos = 1,
			Dolares = 2,
			Reales = 3
		}

		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		[Key, Column(Order = 0)]
		public int ID { get; set; }
		public int SocioId { get; set; }

		[ForeignKey("SocioId")]
		public virtual Socio Socio { get; set; }
		public int AñoPagado { get; set; }
		public double Monto { get; set; }
        public TipoMonedaEnum TipoMoneda { get; set; }
        public DateTime FechaEmision { get; set; }
        public DateTime FechaVencimiento { get; set; }
        public DateTime? FechaPago { get; set; }

		public object GetId()
		{
			return this.ID;
		}
	}
}