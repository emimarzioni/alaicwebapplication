﻿namespace AlaicWebApplication.Entities
{
    public interface IEntity
    {
        object GetId();
    }
}
