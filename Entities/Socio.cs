﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Identity;

namespace AlaicWebApplication.Entities
{
	[Table("Socio")]
	public class Socio : IEntity
	{
		public enum AsociacionNacionalEnum
		{
			Sí = 1,
			No = 2
		}

		public enum PaisEnum
		{
			Argentina = 1,
			Brasil = 2,
			Perú = 3,
			Chile = 4,
			Uruguay = 5,
			Paraguay = 6,
			Venezuela = 7,
			Ecuador = 8,
			Colombia = 9
		}

		public enum EstadoEnum
		{
			Activo = 1,
			Vitalicio = 2,
			Inactivo = 3
		}

		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		[Key, Column(Order =0)]
		public int Id { get; set; }

		[Required]
		public string Apellido { get; set; }

		[Required]
		public string Nombres { get; set; }
		public char Sexo { get; set; }
		public DateTime FechaNacimiento { get; set; }
		public string Domicilio { get; set; }
		public EstadoEnum CodigoEstado { get; set; }
		public int AñoIncorporacion { get; set; }
		public string Email { get; set; }
        public int InstitucionId { get; set; }
		public string EmailAlternativo { get; set; }
		public PaisEnum Pais { get; set; }
		public AsociacionNacionalEnum EsAsociacionNacional { get; set; }
        public virtual Institucion Institucion { get; set; }
        public virtual IList<Cuota> Cuotas { get; set; }

		public object GetId()
		{
			return this.Id;
		}
	}

}